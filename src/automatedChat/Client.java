package automatedChat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;

public class Client {
    private static final String SERVER_IP = "127.0.0.1";
    private static final int SERVER_PORT = 9090;

    public static void main(String[] args) throws IOException {
        Socket socket = new Socket(SERVER_IP, SERVER_PORT);

        ServerConnection serverConn = new ServerConnection(socket);

        BufferedReader keyboard = new BufferedReader(new InputStreamReader(System.in));
        PrintWriter output = new PrintWriter(socket.getOutputStream(), true);

        new Thread(serverConn).start();

        String welcomeClient = serverConn.getInput().readLine();
        System.out.println(welcomeClient);

        System.out.println("> ");
        while (true) {
            String command = keyboard.readLine();
            output.println(command);

            if ("quit".equals(command)) {
                System.out.println("Quiting...");
                break;
            }
        }

        socket.close();
        System.exit(0);
    }
}
