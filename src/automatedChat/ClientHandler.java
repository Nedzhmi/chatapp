package automatedChat;

import java.io.*;
import java.net.Socket;
import java.nio.file.Files;
import java.nio.file.Path;
import java.util.ArrayList;
import java.util.List;

public class ClientHandler implements Runnable {
    private static final String FILE_PATH = "chat.txt";
    private static List<String> chatHistory = new ArrayList<>();
    private static final String[] firstNames = {"Georgi", "Ivan", "Dimitar", "Nikolai", "Alex", "Miroslav",
            "Stefan", "Yordan", "Veselin", "Lyubomir"};
    private static final String[] lastNames = {"Petrov", "Mihaylov", "Bojinov", "Stefanov", "Vasilev",
            "Nikolov", "Atanasov", "Petkov", "Alexandrov"};

    private Socket client;
    private ArrayList<ClientHandler> clients;
    private BufferedReader input;
    private PrintWriter output;
    private String clientName;
    private static int loggedClients;
    private boolean isActive;
    private Server server;

    public ClientHandler(Socket client, ArrayList<ClientHandler> clients, Server server) throws IOException {
        this.client = client;
        this.clients = clients;
        this.input = new BufferedReader(new InputStreamReader(client.getInputStream()));
        this.output = new PrintWriter(client.getOutputStream(), true);
        this.clientName = getRandomName();
        loggedClients++;
        this.isActive = false;
        this.server = server;
    }

    public String getClientName() {
        return this.clientName;
    }

    public static int getLoggedClients() {
        return loggedClients;
    }

    @Override
    public void run() {
        try {
            this.isActive = true;
            output.println("Welcome, " + this.clientName + "!");
            while (this.isActive) {
                String request = input.readLine();
                if (request.startsWith("say")) {
                    int firstSpaceIndex = request.indexOf(" ");

                    if (firstSpaceIndex != -1) {
                        String message = request.substring(firstSpaceIndex).trim();
                        String messageToSend = getClientName() + ": " + message;

                        if (!message.equals("")) {
                            outputToAll(messageToSend);
                            chatHistory = readFile();
                            chatHistory.add(messageToSend);
                            writeToFile();
                        }
                    }

                } else if (request.startsWith("search")) {
                    int firstSpaceIndex = request.indexOf(" ");

                    if (firstSpaceIndex != -1) {
                        chatHistory = readFile();
                        String resultString = request.substring(firstSpaceIndex).trim();

                        StringBuilder sb = new StringBuilder();

                        for (String message : chatHistory) {
                            String[] splitMessage = message.split(":");
                            String textPart = splitMessage[1];
                            if (textPart.contains(resultString)) {
                                sb.append(message).append(System.lineSeparator());
                            }
                        }
                        sb.append("> ");
                        String outputString = sb.toString().trim();

                        if (outputString.equals(">")) {
                            this.output.println(String.format("No matches for '%s'!%n> ", resultString));
                        } else {
                            this.output.println(sb.toString());
                        }
                    }

                } else if ("get online".equalsIgnoreCase(request)) {
                    ArrayList<String> activeUsers = server.getActiveUsers();
                    StringBuilder sb = new StringBuilder();
                    for (String activeUser : activeUsers) {
                        sb.append(activeUser).append(System.lineSeparator());
                    }
                    sb.append("> ");
                    String activeUsersOutput = sb.toString().trim();

                    if (activeUsersOutput.equals("")) {
                        this.output.println("No active users!");
                    } else {
                        this.output.println("Active users: ");
                        this.output.println(activeUsersOutput);
                    }


                } else if ("quit".equalsIgnoreCase(request)) {
                    this.isActive = false;
                    break;
                } else {
                    this.output.println("Invalid input!");
                }
            }
        } catch (IOException | NullPointerException ex) {
            System.out.println("Exception while executing the Client Handler " + ex.getMessage());
        } finally {
            server.getActiveUsers().remove(this.clientName);
            System.out.println("[SERVER] " + this.clientName + " left the server.");
            if (!server.getWaitingQueue().isEmpty()) {
                String joiningClient = server.getWaitingQueue().poll();
                server.getActiveUsers().add(joiningClient);
                System.out.println("[SERVER] " + joiningClient + " joined the server!");
            }
            loggedClients--;
            isActive = false;
            output.close();
            try {
                client.close();
                input.close();
            } catch (IOException ex) {
                System.out.println("Exception while closing resources in Client Handler" + ex.getMessage());
            }
        }
    }

    private void outputToAll(String message) {
        for (ClientHandler client : clients) {
            if (client.isActive) {
                client.output.println(message);
                client.output.println("> ");
            }
        }
    }

    public static String getRandomName() {
        String firstName = firstNames[(int) (Math.random() * firstNames.length)];
        String lastName = lastNames[(int) (Math.random() * lastNames.length)];
        return firstName + " " + lastName;
    }

    private void writeToFile() throws FileNotFoundException {
        PrintWriter writer;
        try {
            writer = new PrintWriter(FILE_PATH);
        } catch (FileNotFoundException e) {
            throw new FileNotFoundException();
        }
        for (String line : chatHistory) {
            writer.println(line);
        }
        writer.close();
    }

    private List<String> readFile() throws IOException {
        Path filePath = Path.of(FILE_PATH);
        if (Files.exists(filePath) && !Files.isDirectory(filePath)) {
            try {
                chatHistory = Files.readAllLines(filePath);
            } catch (IOException e) {
                throw new IOException();
            }
        }

        return chatHistory;
    }
}
