package automatedChat;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.Socket;

public class ServerConnection implements Runnable {
    private Socket server;
    private BufferedReader input;

    public ServerConnection(Socket server) throws IOException {
        this.server = server;
        this.input = new BufferedReader(new InputStreamReader(server.getInputStream()));
    }

    public BufferedReader getInput() {
        return input;
    }

    @Override
    public void run() {
        try {
            while (true) {
                String serverResponse = input.readLine();

                if (serverResponse == null) {
                    break;
                }
                System.out.println(serverResponse);
            }

        } catch (IOException ex) {
            System.out.println("Exception while executing the Server Connection " + ex.getMessage());
        } finally {
            try {
                input.close();
            } catch (IOException ex) {
                System.out.println("Exception while closing resources in Server Connection " + ex.getMessage());
            }
        }
    }
}
