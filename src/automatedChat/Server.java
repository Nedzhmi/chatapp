package automatedChat;

import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayDeque;
import java.util.ArrayList;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

public class Server {
    private static final int PORT = 9090;
    public static final int POOL_COUNT = 3;

    private ArrayList<ClientHandler> clients;
    private ArrayDeque<String> waitingQueue;
    private ArrayList<String> activeUsers;
    private ExecutorService pool;

    public Server(ArrayList<ClientHandler> clients, ArrayDeque<String> waitingQueue, ArrayList<String> activeUsers, ExecutorService pool) {
        this.clients = clients;
        this.waitingQueue = waitingQueue;
        this.activeUsers = activeUsers;
        this.pool = pool;
    }

    public ArrayDeque<String> getWaitingQueue() {
        return waitingQueue;
    }

    public ArrayList<String> getActiveUsers() {
        return activeUsers;
    }

    public static void main(String[] args) throws IOException {
        ServerSocket listener = new ServerSocket(PORT);
        ArrayList<ClientHandler> clients = new ArrayList<>();
        ArrayDeque<String> waitingQueue = new ArrayDeque<>();
        ArrayList<String> activeUsers = new ArrayList<>();
        ExecutorService pool = Executors.newFixedThreadPool(POOL_COUNT);
        Server server = new Server(clients, waitingQueue, activeUsers, pool);

        while (true) {
            System.out.println("[SERVER] Waiting for client connection...");
            Socket client = listener.accept();

            ClientHandler clientThread = new ClientHandler(client, clients, server);
            clients.add(clientThread);
            pool.execute(clientThread);

            if (ClientHandler.getLoggedClients() <= POOL_COUNT) {
                activeUsers.add(clientThread.getClientName());
                System.out.println("[SERVER] " + clientThread.getClientName() + " connected to the server!");
            } else {
                waitingQueue.offer(clientThread.getClientName());
                System.out.println("[SERVER] " + clientThread.getClientName() + " is waiting on the queue!");
            }
        }
    }
}
